# VARIABLE PRIVATE & PROTECTED
class Hewan:
  def __init__(self, nama, jenis):
    # private
    self.__nama = nama
    # protected
    self._jenis = jenis
    # ENCAPSULETION
    # getter
  def infoNama(self):
      return self.__nama
    # setter
  def editNama(self, nama):
      self.__nama = nama
      return self.__nama
        
hewan = Hewan('ayam', 'betina')
print(hewan.infoNama())
print(hewan.editNama('pitik'))
